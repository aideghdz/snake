(function(w, d){
     /**
     * @fileOverview Building the board of the game.
     * @author <a href="mailto:aideghdz@ciencias.com">Aide Garcia</a>
     */

    /**
     *@class Postitions
     *@classdes Model of a point in 2-D
     */
    class Position{
        /**
         * Create a point.
         * @param {number} x - The x value.
         * @param {number} y - The y value.
         */
        constructor(x,y){
            this.x = x;
            this.y = y;

        }
    }
    /**
     *@class Snake
     *@classdes Model of snake
     */
    class Snake {
        /**
         * Create a snake.
         * @param {number} limit1 - The x limit of valid positions.
         * @param {number} limit2 - The y limit of valid positions.
         */
        constructor(limit1, limit2){
        this.posX =  Math.floor(limit1/2);
        this.posY=Math.floor(limit2/2);
        this.direction=1;
        this.size= 0;
        this.positions = [];

      }
      
    }
    /**
    * @constant game_speed
    * @description delay time of the game in miliseconds(Change here to go faster of slower)
    * @type {number}
    * @default 800
    */
     const game_speed = 400;
    /**
    * @constant board
    * @description 2-D array the represent the board of the game.
    * @type {[Object]}
    * @default []
    */
    const board = [];
    /**
    * @constant snake
    * @description the snake object of the game.
    * @type {Snake}
    * @default 
    */
    var snake;
     /**
    * @constant cols
    * @description Number of colums of the board(Change here to customized )
    * @type {Number}
    * @default 64
    */
    const cols = 64;
    /**
    * @constant rows
    * @description Number of rows of the board(Change here to customized)
    * @type {Number}
    * @default 48
    */
    const rows = 48;
    /** 
     * @function initBoard
     * @description Makes the html elements of the  board also
     * the board representation (2d-matrix)
     */
    function initBoard(){
        d.getElementById("play-again").onclick = restart;
        w.addEventListener("keydown", enterKey,false);   
        var boardDiv = d.getElementById("board");
        boardDiv.style.height = (rows *10)+"px"
        boardDiv.style.width = (cols *10)+"px"
        for (var y = 0; y < rows; ++y) {
            var row = [];
            for (var x = 0; x < cols; ++x) {
                var cell = {food:false,snake:false};
                cell.element = d.createElement('div');
                boardDiv.appendChild(cell.element);
                row.push(cell);
            }
            board.push(row);
        }
    }
     /** 
     * @function initGame
     * @description Sets the initial state of the board (food and snake)
     */
    function initGame(){
        snake = new Snake(cols,rows);
        board[snake.posY][snake.posX].snake = true;
        snake.positions.push(new Position(snake.posX,snake.posY));
        placeApple();     
        
    }
    
    /** 
     * @function update
     * @description Updates the status of the board
     */
    function update() {
        //Updating every cell
        for (var y = 0; y < rows; ++y) {
            for (var x = 0; x < cols; ++x) {
                var cell = board[y][x];
                if (cell.snake) {
                    cell.element.className = 'snake';
                }else if (cell.food) {
                    cell.element.className = 'food';
                }
                else {
                    cell.element.className = '';
                }
            }
        }
        //Updatding snake direction.
        switch (snake.direction) {
            case 1:snake.posY--; break;
            case 2:snake.posY++; break;
            case 3:snake.posX--; break;
            case 4:snake.posX++; break;
        }
        //Check if lost 1(walls)
        if (snake.posX < 0 || snake.posY < 0 || snake.posX >= cols || snake.posY >= rows) {
            d.getElementById("end-game").style.display = "block";
        }
        //Check if lost 2 (cannibalism)
        lostCheck();

        if(board[snake.posY][snake.posX].food){
            snake.size ++;
            snake.positions.push(new Position(snake.posX,snake.posY));
            board[snake.posY][snake.posX].food = false;
            d.getElementById("score").innerText = snake.size;
            placeApple();

        }
        updateSnakePos();
        
        
        setTimeout(update, game_speed);
    }

    function lostCheck(){
        if(snake.size > 4){
            for(var i = 1 ; i< snake.positions.length ; i++){
                var pos = snake.positions[i];
                if(snake.posX == pos.x && pos.y == snake.posY ){
                     d.getElementById("end-game").style.display = "block";
                     update.setTimeout();
                }
            }

        }

    }
     /** 
     * @function updateSnakePos
     * @description function that updates the snake list of positions.
     */
    function updateSnakePos(){
        snake.positions.push(new Position(snake.posX,snake.posY));
        board[snake.posY][snake.posX].snake = true;
        var aux = snake.positions.shift();
        board[aux.y][aux.x].snake = false;

    }
     /** 
     * @function enterKey
     * @description function handle the key moves.
     */
    function enterKey(event) {
        switch (event.key) {
            case 'ArrowUp': 
            if(snake.direction != 2){
                snake.direction = 1; 
            }
            break;
            case 'ArrowDown':
             if(snake.direction != 1){
                snake.direction = 2;
             }
              break;
            case 'ArrowLeft':
             if(snake.direction != 4){
                snake.direction = 3; 
             }
             break;
            case 'ArrowRight': 
            if(snake.direction!= 3){
                snake.direction = 4;
            } break;
            default: return;
        }
        event.preventDefault();
    }
     /** 
     * @function restart
     * @description function that reload the game.
     */
    function restart(){
        w.location.reload();
    }
     /** 
     * @function placeApple
     * @description function that place the food (random).
     */
    function placeApple() {
        board[Math.floor(Math.random()*rows)][Math.floor(Math.random()*cols)].food = true;
    }
    //All the calls
    initBoard();
    initGame();
    update();
    
})(window, document);